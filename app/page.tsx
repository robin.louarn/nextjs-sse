"use client";

import { FC, useEffect, useMemo, useState } from "react";

export type MyEvent = { status: string; date: Date };

const Home: FC = () => {
  const [state, setState] = useState<string>();

  useEffect(() => {
    const evtSource = new EventSource("/api");

    evtSource.onmessage = (event: MessageEvent<string>) => {
      console.log(event);
      const myEvent: MyEvent = JSON.parse(event.data);
      setState(myEvent.status);
    };

    evtSource.onopen = (event) => {
      console.log(event);
    };

    evtSource.onerror = () => {
      evtSource.close();
    };

    return () => {
      evtSource.close();
    };
  }, []);

  return (
    <div
      style={{
        display: "flex",
        width: "100vw",
        height: "100vh",
        alignItems: "center",
        justifyContent: "center",
        fontSize: "100px",
      }}
    >
      <p>{state}</p>
    </div>
  );
};

export default Home;
