import { NextResponse } from "next/server";
import {
  ReceiveMessageCommand,
  SQSClient,
  SendMessageCommand,
} from "@aws-sdk/client-sqs";
import { sqs } from "~lib/sqs";

export const POST = async () => {
  const params = new SendMessageCommand({
    QueueUrl: "http://localhost:4566/000000000000/MyQueue",
    MessageBody: "Hello, SQS!",
  });

  try {
    const { MessageId } = await sqs.send(params);
    return NextResponse.json({ messageId: MessageId });
  } catch (error) {
    console.error(error);
  }

  return NextResponse.json({ status: 500 }, { status: 500 });
};

export const GET = async () => {
  const params = new ReceiveMessageCommand({
    QueueUrl: "http://localhost:4566/000000000000/MyQueue",
    MaxNumberOfMessages: 10,
    WaitTimeSeconds: 10,
  });

  try {
    const messages = (await sqs.send(params)).Messages?.map(({ Body }) => Body);
    return NextResponse.json({ messages });
  } catch (error) {
    console.error(error);
  }

  return NextResponse.json({ status: 500 }, { status: 500 });
};
