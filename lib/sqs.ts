import { SQSClient } from "@aws-sdk/client-sqs";

export const sqs = new SQSClient({
  region: "eu-west-1",
  endpoint: "http://localhost:4566",
});
