import { NextResponse } from "next/server";
import { prisma } from "~lib/prisma";
import { faker } from "@faker-js/faker";

export const POST = async () => {
  const user = await prisma.user.create({
    data: {
      email: faker.internet.email(),
    },
  });

  return NextResponse.json(user);
};
