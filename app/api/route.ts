export const runtime = "edge";

export const GET = async () => {
  const iterator = makeIterator();
  const stream = iteratorToStream(iterator);

  return new Response(stream, {
    headers: {
      Connection: "keep-alive",
      "Content-Encoding": "none",
      "Content-Type": "text/event-stream",
      "Cache-Control": "no-store",
    },
  });
};

const iteratorToStream = (iterator: AsyncGenerator<string, string, never>) => {
  return new ReadableStream({
    pull: async (controller) => {
      const { value, done } = await iterator.next();
      if (value) controller.enqueue(value);
      if (done) controller.close();
    },
  });
};

async function* makeIterator(): AsyncGenerator<string, string, never> {
  let id = 0;

  await sleep(1000);

  yield [
    `id: ${id++}\n`,
    "type: message\n",
    `data: ${JSON.stringify({ status: "pending" })}\n\n`,
  ].join("");

  await sleep(2000);

  yield [
    `id: ${id++}\n`,
    "type: message\n",
    `data: ${JSON.stringify({ status: "pending" })}\n\n`,
  ].join("");

  await sleep(3000);

  return [
    `id: ${id++}\n`,
    "type: message\n",
    `data: ${JSON.stringify({ status: "validated" })}\n`,
    "retry: 1000\n\n",
  ].join("");
}

function sleep(time: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}
